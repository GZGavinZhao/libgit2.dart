import 'dart:ffi';
import 'package:ffi/ffi.dart';

import 'package:libgit2/src/generated_bindings.dart';
import 'package:libgit2/src/libgit2_base.dart';

GitException getLastException(int error) {
  return GitException.fromPointer(error, libgit.git_error_last());
}

class GitException implements Exception {
  final int error;
  final int klass;
  final String message;

  GitException(this.error, this.klass, this.message);

  GitException.fromPointer(this.error, Pointer<git_error> errorPtr)
      : klass = errorPtr.ref.klass,
        message = errorPtr.ref.message.cast<Utf8>().toDartString();
}
