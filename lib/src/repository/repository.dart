import 'dart:ffi';

import 'package:ffi/ffi.dart';

import 'package:libgit2/src/exceptions.dart';
import 'package:libgit2/src/generated_bindings.dart';
import 'package:libgit2/src/libgit2_base.dart';
import 'package:libgit2/src/repository/repository_init_options.dart';
import 'package:libgit2/src/utils/memory.dart';

export 'repository_init_options.dart';

/// Representation of an *existing* git repository, including all its object
/// contents. If you don't have an existing repository, [GitRepository.init]
/// one.
///
/// ```dart
/// var repo = GitRepository.init('myFolder')
/// ```
///
/// This class has automatic memory management, so there's no need to free
/// native resources by the developers.
class GitRepository implements Finalizable {
  final Pointer<Pointer<git_repository>> _repo = calloc();
  final String _path;

  static final _finalizer =
      NativeFinalizer(libgit.addresses.git_repository_free.cast());

  /// Initialize a new repository at the given [path].
  ///
  /// For example, `GitRepository.init('hello')` creates a folder called "hello"
  /// at the path `./hello` and initialize a git repository *within* it.
  GitRepository.init(this._path, {GitRepositoryInitOptions? options}) {
    final pathS = AFString(path);

    final int error = options == null
        ? libgit.git_repository_init(_repo, pathS.ptr.cast(), 0)
        : libgit.git_repository_init_ext(
            _repo,
            pathS.ptr.cast(),
            options.pointer,
          );

    _finalizer.attach(this, _repo.value.cast());
    freenalizer.attach(this, _repo.cast());

    if (error != 0) {
      throw getLastException(error).message;
    }
  }

  /// Opens an *existing* repository at the given [path].
  GitRepository.open(this._path) {
    final pathS = AFString(path);

    final int error = libgit.git_repository_open(
      _repo,
      pathS.ptr.cast(),
    );

    if (error != 0) {
      throw getLastException(error);
    }
  }

  /// Path of the repository
  String get path => _path;

  /// Frees the allocated repository. Don't use this object again once this
  /// method is called.
  ///
  /// Currently the [NativeFinalizer] will automatically free resources for you
  /// when this object runs out of scope.
  ///
  /// Only use this function if you are manually freeing native resources.
  void dispose() {
    libgit.git_repository_free(_repo.value);
    calloc.free(_repo);
  }
}
