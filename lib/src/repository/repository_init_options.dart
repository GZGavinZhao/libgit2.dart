import 'dart:ffi';

import 'package:ffi/ffi.dart';

import 'package:libgit2/src/exceptions.dart';
import 'package:libgit2/src/generated_bindings.dart';
import 'package:libgit2/src/libgit2_base.dart';
import 'package:libgit2/src/utils/memory.dart';

/// Extended options structure for [GitRepository.init].
///
/// This contains extra options for [GitRepository.init] that enable additional
/// initialization features.
class GitRepositoryInitOptions implements Finalizable {
  final Pointer<git_repository_init_options> _pointer = calloc();

  /// Flag options.
  GitRepositoryInitFlag? flag;

  /// Set to one of the standard [GitRepositoryInitMode] constants, or to a
  /// custom value that you would like.
  int mode;

  /// The path to the working dir or `null` by default (i.e. repo_path parent on
  /// non-bare repos). IF THIS IS RELATIVE PATH, IT WILL BE EVALUATED RELATIVE
  /// TO THE REPO_PATH. If this is not the "natural" working directory, a .git
  /// gitlink file will be created here linking to the repo_path.
  String? workdirPath;

  /// If set, this will be used to initialize the "description" file in the
  /// repository, instead of using the template content.
  String? description;

  /// When [GitRepositoryInitFlag.initExternalTemplate] is `true`, this contains
  /// the path to use for the template directory. If this is NULL, the config or
  /// default directory options will be used instead.
  String? templatePath;

  /// The name of the head to point HEAD at. If null, then this will be treated
  /// as "master" and the HEAD ref will be set to "refs/heads/master". If this
  /// begins with "refs/" it will be used verbatim; otherwise "refs/heads/" will
  /// be prefixed
  String? initialHead;

  /// If this is non-null, then after the rest of the repository initialization
  /// is completed, an "origin" remote will be added pointing to this URL.
  String? originUrl;

  /// Pointer to the object that can be passed to a C function.
  Pointer<git_repository_init_options> get pointer => _pointer;

  GitRepositoryInitOptions({
    this.flag,
    this.mode = GitRepositoryInitMode.sharedUmask,
    this.workdirPath,
    this.description,
    this.templatePath,
    this.initialHead,
    this.originUrl,
  }) {
    final int error = libgit.git_repository_init_options_init(
      _pointer,
      GIT_REPOSITORY_INIT_OPTIONS_VERSION,
    );

    if (error != 0) {
      throw getLastException(error);
    }

    freenalizer.attach(this, _pointer.cast());

    if (flag != null) {
      _pointer.ref.flags = flag!.value;
    }
    _pointer.ref.mode = mode;
    if (workdirPath != null) {
      // No need to free these pointers. Either libgit2 or the operating system
      // will deal with them.
      //
      // Plus, if the pointer is freed, the code will not work.
      final wpPtr = workdirPath!.toNativeUtf8().cast<Int8>();
      _pointer.ref.workdir_path = wpPtr;
    }
    if (description != null) {
      final dPtr = description!.toNativeUtf8().cast<Int8>();
      _pointer.ref.description = dPtr;
    }
    if (templatePath != null) {
      final tPtr = templatePath!.toNativeUtf8().cast<Int8>();
      _pointer.ref.template_path = tPtr;
    }
    if (initialHead != null) {
      // final ihPtr = initialHead!.toNativeUtf8().cast<Int8>();
      _pointer.ref.initial_head = AFString(initialHead!).ptr.cast();
    }
    if (originUrl != null) {
      final ouPtr = originUrl!.toNativeUtf8().cast<Int8>();
      _pointer.ref.origin_url = ouPtr;
    }
  }
}

/// Mode options for [GitRepositoryInitOptions].
///
/// Set the [GitRepositoryInitOptions.mode] field of the
/// [GitRepositoryInitOptions] structure either to the custom mode that you
/// would like, or to one of the defined modes.
abstract class GitRepositoryInitMode {
  /// Use "--shared=all" behavior, adding world readability.
  static const sharedAll =
      git_repository_init_mode_t.GIT_REPOSITORY_INIT_SHARED_ALL;

  /// Use "--shared=group" behavior, chmod'ing the new repo to be group writable
  /// and "g+sx" for sticky group assignment.
  static const sharedGroup =
      git_repository_init_mode_t.GIT_REPOSITORY_INIT_SHARED_GROUP;

  /// Use permissions configured by umask - the default.
  static const sharedUmask =
      git_repository_init_mode_t.GIT_REPOSITORY_INIT_SHARED_UMASK;
}

/// Option flags for [GitRepositoryInitOptions].
///
// These flags configure extra behaviors to [GitRepository.init].
// TODO: logic is wrong here.
class GitRepositoryInitFlag {
  /// Create a bare repository with no working directory.
  bool initBare;

  /// Return an GIT_EEXISTS error if the repo_path appears to already be
  /// an git repository.
  bool initNoReinit;

  /// Normally a "/.git/" will be appended to the repo path for
  /// non-bare repos (if it is not already there), but passing this flag
  /// prevents that behavior.
  bool noDotgitDir;

  /// Make the repo_path (and workdir_path) as needed. Init is always willing
  /// to create the ".git" directory even without this flag. This flag tells
  /// init to create the trailing component of the repo and workdir paths
  /// as needed.
  ///
  /// Note: despite the docs above, repo creation will fail if the folder the
  /// user wants to put the repository `in` does not exist! Hence, this value is
  /// set to `true` by default to align with normal usage.
  bool initMkdir;

  /// Recursively make all components of the repo and workdir paths as
  /// necessary.
  bool initMkpath;

  /// libgit2 normally uses internal templates to initialize a new repo.
  /// This flags enables external templates, looking the "template_path" from
  /// the options if set, or the `init.templatedir` global config if not,
  /// or falling back on "/usr/share/git-core/templates" if it exists.
  bool initExternalTemplate;

  /// If an alternate workdir is specified, use relative paths for the gitdir
  /// and core.worktree.
  bool initRelativeGitlink;

  GitRepositoryInitFlag({
    this.initBare = false,
    this.initNoReinit = false,
    this.noDotgitDir = false,
    this.initMkdir = true,
    this.initMkpath = false,
    this.initExternalTemplate = false,
    this.initRelativeGitlink = false,
  });

  int get value {
    int result = 0;
    if (initBare) {
      result |= git_repository_init_flag_t.GIT_REPOSITORY_INIT_BARE;
    }
    if (initNoReinit) {
      result |= git_repository_init_flag_t.GIT_REPOSITORY_INIT_NO_REINIT;
    }
    if (noDotgitDir) {
      result |= git_repository_init_flag_t.GIT_REPOSITORY_INIT_NO_DOTGIT_DIR;
    }
    if (initMkdir) {
      result |= git_repository_init_flag_t.GIT_REPOSITORY_INIT_MKDIR;
    }
    if (initMkpath) {
      result |= git_repository_init_flag_t.GIT_REPOSITORY_INIT_MKPATH;
    }
    if (initExternalTemplate) {
      result |=
          git_repository_init_flag_t.GIT_REPOSITORY_INIT_EXTERNAL_TEMPLATE;
    }
    if (initRelativeGitlink) {
      result |= git_repository_init_flag_t.GIT_REPOSITORY_INIT_RELATIVE_GITLINK;
    }

    return result;
  }
}
