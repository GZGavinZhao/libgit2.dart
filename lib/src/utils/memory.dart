import 'dart:ffi';

import 'package:ffi/ffi.dart';

/// A generic [NativeFinalizer] to free any pointer by calling the C function
/// `free`. It's kind of like a workaround to combat the restriction of
/// [NativeFinalizer] (which only accepts a C function, not a Dart
/// function).
final freenalizer = NativeFinalizer(DynamicLibrary.executable().lookup('free'));

/// Creates a wrapper around a String so that its corresponding [Pointer] will
/// be automatically freed using [NativeFinalizer]. Hence its name,
/// _AutoFreeableString_.
class AFString implements Finalizable {
  final String content;
  final Pointer ptr;

  AFString(this.content, {bool utf16 = false})
      : ptr = utf16 ? content.toNativeUtf16() : content.toNativeUtf8() {
    freenalizer.attach(this, ptr.cast());
  }
}
