import 'dart:ffi';

import 'package:libgit2/src/generated_bindings.dart';

late LibGit2Ffi libgit;

// TODO: check how to implement custom Exception. Maybe find some examples.
/// Initialize LibGit2. Must be called before any `libgit2` apis are used.
///
/// This function must be called before any other libgit2 function in order to
/// set up global state and threading.
///
/// This function may be called multiple times - it will return the number of
/// times the initialization has been called (including this one) that have not
/// subsequently been shutdown.
int initLibGit2({String? path}) {
  // TODO: get the different paths, like OS-specific ones.
  libgit = LibGit2Ffi(DynamicLibrary.open(path ?? ''));

  final error = libgit.git_libgit2_init();
  if (error < 1) {
    throw LibGit2InitException(error);
  }

  return error;
}

/// Shutdown libgit2 and free resources.
///
/// Cleans up the global state and threading context after calling it as many
/// times as [initLibGit2] was called - it will return the number of
/// remainining initializations that have not been shutdown (after this one).
///
/// Usually, there's no need to call this function because the operating system
/// will do the resource cleanup. However, if an application uses libgit2 in
/// some areas which are not usually active, this function can be called.
int shutdownLibGit2() {
  return libgit.git_libgit2_shutdown();
}

/// Checks if you are awesome. Spoiler: you are.
class Awesome {
  bool get isAwesome => true;
}

class LibGit2InitException implements Exception {
  final int errorCode;

  LibGit2InitException(this.errorCode);
}
