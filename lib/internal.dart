/// Raw bindings for `libgit2`.
library libgit2.internal;

export 'src/generated_bindings.dart';
export 'src/version.dart';
