import 'package:grinder/grinder.dart';

void main(List<String> args) => grind(args);

@Task()
Future<void> test() => TestRunner().testAsync();

// @DefaultTask()
@Task()
@Depends(test)
void build() {
  Pub.build();
}

@Task()
Future<void> buildCmake() async {
  // final configure = await Process.start(
  //   'cmake',
  //   [
  //     '-B',
  //     'build',
  //     '-DBUILD_CLAR=OFF',
  //     'libgit2',
  //   ],
  //   mode: ProcessStartMode.inheritStdio,
  // );

  await runAsync(
    'cmake',
    arguments: [
      '-B',
      'build',
      '-DBUILD_CLAR=OFF',
      'libgit2',
    ],
  );

  await runAsync(
    'cmake',
    arguments: ['--build', 'build'],
  );

  // int exitCode = await configure.exitCode;
  // if (exitCode != 0) {
  //   exit(exitCode);
  // }

  // final build = await Process.start(
  //   'cmake',
  //   ['--build', 'build'],
  //   mode: ProcessStartMode.inheritStdio,
  // );

  // exitCode = await build.exitCode;
  // if (exitCode != 0) {
  //   exit(exitCode);
  // }
}

@Task()
void clean() {
  defaultClean();
}
