import 'dart:io';

Future<void> main(List<String> args) async {
  final configure = await Process.start(
    'cmake',
    [
      '-B',
      'build',
      '-DBUILD_CLAR=OFF',
      'libgit2',
    ],
    mode: ProcessStartMode.inheritStdio,
  );

  int exitCode = await configure.exitCode;
  if (exitCode != 0) {
    exit(exitCode);
  }

  final build = await Process.start(
    'cmake',
    ['--build', 'build'],
    mode: ProcessStartMode.inheritStdio,
  );

  exitCode = await build.exitCode;
  if (exitCode != 0) {
    exit(exitCode);
  }
}
