#include <stdio.h>
#include <git2.h>

int main(int argc, char const *argv[])
{
	git_libgit2_init();

	git_repository *repo = NULL;

	printf("%d", git_repository_open(&repo, "."));

	git_libgit2_shutdown();

	return 0;
}