import 'dart:io';
import 'dart:math';

import 'package:dylib/dylib.dart';
import 'package:libgit2/libgit2.dart';
import 'package:path/path.dart' as p;

void main(List<String> args) {
  initLibGit2(path: p.join(p.current, 'build', resolveDylibName('libgit2')));

  GitRepository.init('/tmp/bruh+${Random().nextInt(1000)}').dispose();
  stderr.writeln('Default creation succeeded!');
  final repo = GitRepository.init(
    'bruh+${Random().nextInt(1000)}',
    options: GitRepositoryInitOptions(
      initialHead: 'main',
    ),
  );
  stdout.writeln(repo.path);
  repo.dispose();

  libgit.git_libgit2_shutdown();
}
