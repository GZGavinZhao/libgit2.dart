// ignore_for_file: avoid_print

import 'dart:ffi';
import 'dart:math';

import 'package:dylib/dylib.dart';
import 'package:ffi/ffi.dart';
import 'package:libgit2/internal.dart';
import 'package:libgit2/libgit2.dart';
import 'package:path/path.dart' as p;

final libgit = LibGit2Ffi(
  DynamicLibrary.open(
    p.join(p.current, 'build', resolveDylibName('libgit2')),
    // p.join('/usr/lib64/', resolveDylibName('libgit2')),
  ),
);

void fetchLastError(String message) {
  final Pointer<git_error> e = libgit.git_error_last();
  print(
    'klass: ${e.ref.klass}, message: ${e.ref.message.cast<Utf8>().toDartString()}',
  );
  // This causes problems on MacOS.
  // Maybe the Dart garbage collector actually does some memory freeing for me?
  // calloc.free(e);
}

void main(List<String> args) {
  final awesome = Awesome();
  print('awesome: ${awesome.isAwesome}');

  final Pointer<Pointer<git_repository>> repo =
      calloc<Pointer<git_repository>>();
  final location =
      (args.isEmpty ? '/tmp/bruh-${Random().nextInt(100)}' : args[0])
          .toNativeUtf8()
          .cast<Int8>();
  // This function causes memory leaks!
  // Valgrind shows that "definitely lost: 40 bytes in 1 blocks"
  if (libgit.git_libgit2_init() > 0) {
    print('Initialized libgit2!');
  }

  final int error = libgit.git_repository_init(
    repo,
    location,
    0,
  );
  if (error != 0) {
    fetchLastError('Error: $error, ');
  } else {
    print('Successfully created repo! $error');
  }

//  final thisRepoLocation = '.'.toNativeUtf8().cast<Int8>();
//  error = libgit.git_repository_open(
//		repo,
//    thisRepoLocation,
//  );
//  if (error != 0) {
//		fetchLastError('Error: $error, ');
//  } else {
//    print('Successfully opened repo! $error');
//  }
//
//  const String url = 'https://github.com/GZGavinZhao/GZGavinZhao.git';
//  const String path = '/tmp/GZGavinZhao';
//  final urlPtr = url.toNativeUtf8();
//  final pathPtr = path.toNativeUtf8();
//  error = libgit.git_clone(
//    repo,
//    urlPtr.cast(),
//    pathPtr.cast(),
//    nullptr,
//  );
//  if (error != 0) {
//		fetchLastError('Error: $error, ');
//  } else {
//    print('Successfully cloned repo! $error');
//  }
//	malloc.free(urlPtr);
//	malloc.free(pathPtr);
//
//	libgit.git_repository_free(repo.value);
  malloc.free(location);
  // libgit.git_repository_free(repo.value); // Doesn't do anything!
  calloc.free(repo);

  print('Trying to shutdown libgit2...');
  if (libgit.git_libgit2_shutdown() == 0) {
    print('libgit2 successfully shutdown!');
  }

//  print('Freeing resources...');
//	malloc.free(repo);
//  malloc.free(location);
//  malloc.free(thisRepoLocation);
//  print('Done!');
}
